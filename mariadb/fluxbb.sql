-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : db
-- Généré le : dim. 14 nov. 2021 à 17:19
-- Version du serveur : 10.6.3-MariaDB-1:10.6.3+maria~focal
-- Version de PHP : 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `fluxbb`
--
CREATE DATABASE IF NOT EXISTS `fluxbb` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `fluxbb`;

-- --------------------------------------------------------

--
-- Structure de la table `forum_bans`
--

DROP TABLE IF EXISTS `forum_bans`;
CREATE TABLE `forum_bans` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(200) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `expire` int(10) UNSIGNED DEFAULT NULL,
  `ban_creator` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `forum_categories`
--

DROP TABLE IF EXISTS `forum_categories`;
CREATE TABLE `forum_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(80) NOT NULL DEFAULT 'New Category',
  `disp_position` int(10) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `forum_categories`
--

INSERT INTO `forum_categories` (`id`, `cat_name`, `disp_position`) VALUES
(1, 'Catégorie de test', 1);

-- --------------------------------------------------------

--
-- Structure de la table `forum_censoring`
--

DROP TABLE IF EXISTS `forum_censoring`;
CREATE TABLE `forum_censoring` (
  `id` int(10) UNSIGNED NOT NULL,
  `search_for` varchar(60) NOT NULL DEFAULT '',
  `replace_with` varchar(60) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `forum_config`
--

DROP TABLE IF EXISTS `forum_config`;
CREATE TABLE `forum_config` (
  `conf_name` varchar(255) NOT NULL DEFAULT '',
  `conf_value` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `forum_config`
--

INSERT INTO `forum_config` (`conf_name`, `conf_value`) VALUES
('o_cur_version', '1.5.9'),
('o_database_revision', '21'),
('o_searchindex_revision', '2'),
('o_parser_revision', '2'),
('o_board_title', 'Forum Ubuntu-fr.org'),
('o_board_desc', 'Communauté francophone d\'utilisateurs d\'Ubuntu'),
('o_default_timezone', '0'),
('o_time_format', 'H:i:s'),
('o_date_format', 'Y-m-d'),
('o_timeout_visit', '1800'),
('o_timeout_online', '300'),
('o_redirect_delay', '1'),
('o_show_version', '0'),
('o_show_user_info', '1'),
('o_show_post_count', '1'),
('o_signatures', '1'),
('o_smilies', '1'),
('o_smilies_sig', '1'),
('o_make_links', '1'),
('o_default_lang', 'Francais'),
('o_default_style', 'Air'),
('o_default_user_group', '4'),
('o_topic_review', '15'),
('o_disp_topics_default', '30'),
('o_disp_posts_default', '25'),
('o_indent_num_spaces', '4'),
('o_quote_depth', '3'),
('o_quickpost', '1'),
('o_users_online', '1'),
('o_censoring', '0'),
('o_show_dot', '0'),
('o_topic_views', '1'),
('o_quickjump', '1'),
('o_gzip', '0'),
('o_additional_navlinks', ''),
('o_report_method', '0'),
('o_regs_report', '0'),
('o_default_email_setting', '1'),
('o_mailing_list', 'admin@ubuntu-fr.org'),
('o_avatars', '1'),
('o_avatars_dir', 'img/avatars'),
('o_avatars_width', '60'),
('o_avatars_height', '60'),
('o_avatars_size', '10240'),
('o_search_all_forums', '1'),
('o_base_url', 'http://fluxbb.localhost'),
('o_admin_email', 'admin@ubuntu-fr.org'),
('o_webmaster_email', 'admin@ubuntu-fr.org'),
('o_forum_subscriptions', '1'),
('o_topic_subscriptions', '1'),
('o_smtp_host', NULL),
('o_smtp_user', NULL),
('o_smtp_pass', NULL),
('o_smtp_ssl', '0'),
('o_regs_allow', '1'),
('o_regs_verify', '0'),
('o_announcement', '0'),
('o_announcement_message', 'Indiquer votre annonce ici.'),
('o_rules', '0'),
('o_rules_message', 'Indiquer vos règles ici'),
('o_maintenance', '0'),
('o_maintenance_message', 'Les forums sont temporairement inaccessibles pour maintenance. Veuillez essayer à nouveau dans quelques minutes.'),
('o_default_dst', '0'),
('o_feed_type', '2'),
('o_feed_ttl', '0'),
('p_message_bbcode', '1'),
('p_message_img_tag', '1'),
('p_message_all_caps', '1'),
('p_subject_all_caps', '1'),
('p_sig_all_caps', '1'),
('p_sig_bbcode', '1'),
('p_sig_img_tag', '0'),
('p_sig_length', '400'),
('p_sig_lines', '4'),
('p_allow_banned_email', '1'),
('p_allow_dupe_email', '0'),
('p_force_guest_email', '1');

-- --------------------------------------------------------

--
-- Structure de la table `forum_forums`
--

DROP TABLE IF EXISTS `forum_forums`;
CREATE TABLE `forum_forums` (
  `id` int(10) UNSIGNED NOT NULL,
  `forum_name` varchar(80) NOT NULL DEFAULT 'New forum',
  `forum_desc` text DEFAULT NULL,
  `redirect_url` varchar(100) DEFAULT NULL,
  `moderators` text DEFAULT NULL,
  `num_topics` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `num_posts` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `last_post` int(10) UNSIGNED DEFAULT NULL,
  `last_post_id` int(10) UNSIGNED DEFAULT NULL,
  `last_poster` varchar(200) DEFAULT NULL,
  `sort_by` tinyint(1) NOT NULL DEFAULT 0,
  `disp_position` int(10) NOT NULL DEFAULT 0,
  `cat_id` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `forum_forums`
--

INSERT INTO `forum_forums` (`id`, `forum_name`, `forum_desc`, `redirect_url`, `moderators`, `num_topics`, `num_posts`, `last_post`, `last_post_id`, `last_poster`, `sort_by`, `disp_position`, `cat_id`) VALUES
(1, 'Forum de test', 'Ceci n\'est qu\'un forum de test.', NULL, NULL, 1, 1, 1636819415, 1, 'admin', 0, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `forum_forum_perms`
--

DROP TABLE IF EXISTS `forum_forum_perms`;
CREATE TABLE `forum_forum_perms` (
  `group_id` int(10) NOT NULL DEFAULT 0,
  `forum_id` int(10) NOT NULL DEFAULT 0,
  `read_forum` tinyint(1) NOT NULL DEFAULT 1,
  `post_replies` tinyint(1) NOT NULL DEFAULT 1,
  `post_topics` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `forum_forum_subscriptions`
--

DROP TABLE IF EXISTS `forum_forum_subscriptions`;
CREATE TABLE `forum_forum_subscriptions` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `forum_id` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `forum_groups`
--

DROP TABLE IF EXISTS `forum_groups`;
CREATE TABLE `forum_groups` (
  `g_id` int(10) UNSIGNED NOT NULL,
  `g_title` varchar(50) NOT NULL DEFAULT '',
  `g_user_title` varchar(50) DEFAULT NULL,
  `g_promote_min_posts` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `g_promote_next_group` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `g_moderator` tinyint(1) NOT NULL DEFAULT 0,
  `g_mod_edit_users` tinyint(1) NOT NULL DEFAULT 0,
  `g_mod_rename_users` tinyint(1) NOT NULL DEFAULT 0,
  `g_mod_change_passwords` tinyint(1) NOT NULL DEFAULT 0,
  `g_mod_ban_users` tinyint(1) NOT NULL DEFAULT 0,
  `g_mod_promote_users` tinyint(1) NOT NULL DEFAULT 0,
  `g_read_board` tinyint(1) NOT NULL DEFAULT 1,
  `g_view_users` tinyint(1) NOT NULL DEFAULT 1,
  `g_post_replies` tinyint(1) NOT NULL DEFAULT 1,
  `g_post_topics` tinyint(1) NOT NULL DEFAULT 1,
  `g_edit_posts` tinyint(1) NOT NULL DEFAULT 1,
  `g_delete_posts` tinyint(1) NOT NULL DEFAULT 1,
  `g_delete_topics` tinyint(1) NOT NULL DEFAULT 1,
  `g_post_links` tinyint(1) NOT NULL DEFAULT 1,
  `g_set_title` tinyint(1) NOT NULL DEFAULT 1,
  `g_search` tinyint(1) NOT NULL DEFAULT 1,
  `g_search_users` tinyint(1) NOT NULL DEFAULT 1,
  `g_send_email` tinyint(1) NOT NULL DEFAULT 1,
  `g_post_flood` smallint(6) NOT NULL DEFAULT 30,
  `g_search_flood` smallint(6) NOT NULL DEFAULT 30,
  `g_email_flood` smallint(6) NOT NULL DEFAULT 60,
  `g_report_flood` smallint(6) NOT NULL DEFAULT 60
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `forum_groups`
--

INSERT INTO `forum_groups` (`g_id`, `g_title`, `g_user_title`, `g_promote_min_posts`, `g_promote_next_group`, `g_moderator`, `g_mod_edit_users`, `g_mod_rename_users`, `g_mod_change_passwords`, `g_mod_ban_users`, `g_mod_promote_users`, `g_read_board`, `g_view_users`, `g_post_replies`, `g_post_topics`, `g_edit_posts`, `g_delete_posts`, `g_delete_topics`, `g_post_links`, `g_set_title`, `g_search`, `g_search_users`, `g_send_email`, `g_post_flood`, `g_search_flood`, `g_email_flood`, `g_report_flood`) VALUES
(1, 'Administrateurs', 'Administrateur', 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0),
(2, 'Modérateurs', 'Modérateur', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0),
(3, 'Invités', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 60, 30, 0, 0),
(4, 'Membres', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 60, 30, 60, 60),
(5, 'AdminWiki', 'AdminWiki', 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 60, 30, 60, 60),
(6, 'Expert Wiki', 'Expert Wiki', 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 60, 30, 60, 60);

-- --------------------------------------------------------

--
-- Structure de la table `forum_online`
--

DROP TABLE IF EXISTS `forum_online`;
CREATE TABLE `forum_online` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `ident` varchar(200) NOT NULL DEFAULT '',
  `logged` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `idle` tinyint(1) NOT NULL DEFAULT 0,
  `last_post` int(10) UNSIGNED DEFAULT NULL,
  `last_search` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `forum_online`
--

INSERT INTO `forum_online` (`user_id`, `ident`, `logged`, `idle`, `last_post`, `last_search`) VALUES
(2, 'admin', 1636822528, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `forum_posts`
--

DROP TABLE IF EXISTS `forum_posts`;
CREATE TABLE `forum_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `poster` varchar(200) NOT NULL DEFAULT '',
  `poster_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `poster_ip` varchar(39) DEFAULT NULL,
  `poster_email` varchar(80) DEFAULT NULL,
  `message` mediumtext DEFAULT NULL,
  `hide_smilies` tinyint(1) NOT NULL DEFAULT 0,
  `posted` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `edited` int(10) UNSIGNED DEFAULT NULL,
  `edited_by` varchar(200) DEFAULT NULL,
  `topic_id` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `forum_posts`
--

INSERT INTO `forum_posts` (`id`, `poster`, `poster_id`, `poster_ip`, `poster_email`, `message`, `hide_smilies`, `posted`, `edited`, `edited_by`, `topic_id`) VALUES
(1, 'admin', 2, '172.18.0.3', NULL, 'Si vous lisez ceci (ce que vous faites probablement), l\'installation de FluxBB semble avoir fonctionné ! A présent, identifiez-vous et rendez-vous au panneau d\'administration pour paramétrer vos forums.', 0, 1636819415, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `forum_reports`
--

DROP TABLE IF EXISTS `forum_reports`;
CREATE TABLE `forum_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `topic_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `forum_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `reported_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `message` text DEFAULT NULL,
  `zapped` int(10) UNSIGNED DEFAULT NULL,
  `zapped_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `forum_search_cache`
--

DROP TABLE IF EXISTS `forum_search_cache`;
CREATE TABLE `forum_search_cache` (
  `id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `ident` varchar(200) NOT NULL DEFAULT '',
  `search_data` mediumtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `forum_search_matches`
--

DROP TABLE IF EXISTS `forum_search_matches`;
CREATE TABLE `forum_search_matches` (
  `post_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `word_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `subject_match` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `forum_search_matches`
--

INSERT INTO `forum_search_matches` (`post_id`, `word_id`, `subject_match`) VALUES
(1, 1, 0),
(1, 2, 0),
(1, 3, 0),
(1, 4, 0),
(1, 5, 0),
(1, 6, 0),
(1, 7, 0),
(1, 8, 0),
(1, 9, 0),
(1, 10, 0),
(1, 11, 0),
(1, 12, 0),
(1, 13, 0),
(1, 14, 0),
(1, 15, 0),
(1, 16, 1),
(1, 17, 1);

-- --------------------------------------------------------

--
-- Structure de la table `forum_search_words`
--

DROP TABLE IF EXISTS `forum_search_words`;
CREATE TABLE `forum_search_words` (
  `id` int(10) UNSIGNED NOT NULL,
  `word` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `forum_search_words`
--

INSERT INTO `forum_search_words` (`id`, `word`) VALUES
(1, 'lisez'),
(2, 'faites'),
(3, 'probablement'),
(4, 'l\'installation'),
(5, 'fluxbb'),
(6, 'semble'),
(7, 'avoir'),
(8, 'fonctionné'),
(9, 'présent'),
(10, 'identifiez-vous'),
(11, 'rendez-vous'),
(12, 'panneau'),
(13, 'd\'administration'),
(14, 'paramétrer'),
(15, 'forums'),
(16, 'message'),
(17, 'test');

-- --------------------------------------------------------

--
-- Structure de la table `forum_topics`
--

DROP TABLE IF EXISTS `forum_topics`;
CREATE TABLE `forum_topics` (
  `id` int(10) UNSIGNED NOT NULL,
  `poster` varchar(200) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `posted` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `first_post_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `last_post` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `last_post_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `last_poster` varchar(200) DEFAULT NULL,
  `num_views` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `num_replies` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `closed` tinyint(1) NOT NULL DEFAULT 0,
  `sticky` tinyint(1) NOT NULL DEFAULT 0,
  `moved_to` int(10) UNSIGNED DEFAULT NULL,
  `forum_id` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `forum_topics`
--

INSERT INTO `forum_topics` (`id`, `poster`, `subject`, `posted`, `first_post_id`, `last_post`, `last_post_id`, `last_poster`, `num_views`, `num_replies`, `closed`, `sticky`, `moved_to`, `forum_id`) VALUES
(1, 'admin', 'Message de test', 1636819415, 1, 1636819415, 1, 'admin', 0, 0, 0, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `forum_topic_subscriptions`
--

DROP TABLE IF EXISTS `forum_topic_subscriptions`;
CREATE TABLE `forum_topic_subscriptions` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `topic_id` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `forum_users`
--

DROP TABLE IF EXISTS `forum_users`;
CREATE TABLE `forum_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT 3,
  `username` varchar(200) NOT NULL DEFAULT '',
  `password` varchar(40) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(50) DEFAULT NULL,
  `realname` varchar(40) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `jabber` varchar(80) DEFAULT NULL,
  `icq` varchar(12) DEFAULT NULL,
  `msn` varchar(80) DEFAULT NULL,
  `aim` varchar(30) DEFAULT NULL,
  `yahoo` varchar(30) DEFAULT NULL,
  `location` varchar(30) DEFAULT NULL,
  `signature` text DEFAULT NULL,
  `disp_topics` tinyint(3) UNSIGNED DEFAULT NULL,
  `disp_posts` tinyint(3) UNSIGNED DEFAULT NULL,
  `email_setting` tinyint(1) NOT NULL DEFAULT 1,
  `notify_with_post` tinyint(1) NOT NULL DEFAULT 0,
  `auto_notify` tinyint(1) NOT NULL DEFAULT 0,
  `show_smilies` tinyint(1) NOT NULL DEFAULT 1,
  `show_img` tinyint(1) NOT NULL DEFAULT 1,
  `show_img_sig` tinyint(1) NOT NULL DEFAULT 1,
  `show_avatars` tinyint(1) NOT NULL DEFAULT 1,
  `show_sig` tinyint(1) NOT NULL DEFAULT 1,
  `timezone` float NOT NULL DEFAULT 0,
  `dst` tinyint(1) NOT NULL DEFAULT 0,
  `time_format` tinyint(1) NOT NULL DEFAULT 0,
  `date_format` tinyint(1) NOT NULL DEFAULT 0,
  `language` varchar(25) NOT NULL DEFAULT 'Francais',
  `style` varchar(25) NOT NULL DEFAULT 'Air',
  `num_posts` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `last_post` int(10) UNSIGNED DEFAULT NULL,
  `last_search` int(10) UNSIGNED DEFAULT NULL,
  `last_email_sent` int(10) UNSIGNED DEFAULT NULL,
  `last_report_sent` int(10) UNSIGNED DEFAULT NULL,
  `registered` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `registration_ip` varchar(39) NOT NULL DEFAULT '0.0.0.0',
  `last_visit` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `admin_note` varchar(30) DEFAULT NULL,
  `activate_string` varchar(80) DEFAULT NULL,
  `activate_key` varchar(8) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `forum_users`
--

INSERT INTO `forum_users` (`id`, `group_id`, `username`, `password`, `email`, `title`, `realname`, `url`, `jabber`, `icq`, `msn`, `aim`, `yahoo`, `location`, `signature`, `disp_topics`, `disp_posts`, `email_setting`, `notify_with_post`, `auto_notify`, `show_smilies`, `show_img`, `show_img_sig`, `show_avatars`, `show_sig`, `timezone`, `dst`, `time_format`, `date_format`, `language`, `style`, `num_posts`, `last_post`, `last_search`, `last_email_sent`, `last_report_sent`, `registered`, `registration_ip`, `last_visit`, `admin_note`, `activate_string`, `activate_key`) VALUES
(1, 3, 'Invité', 'Invité', 'Invité', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 'Francais', 'Air', 0, NULL, NULL, NULL, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL),
(2, 1, 'admin', '4718737b9f2f6e2c225fe605d6c7234330e7e7e4', 'admin@ubuntu-fr.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 'Francais', 'Air', 1, 1636819415, NULL, NULL, NULL, 1636819415, '172.18.0.3', 1636822139, NULL, NULL, NULL),
(3, 5, 'AdminWiki', '8fcd589570e3890b4d83796584e64c614e687101', 'adminwiki@ubuntu-fr.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 'Francais', 'Air', 0, NULL, NULL, NULL, NULL, 1636825658, '172.18.0.3', 1636825659, NULL, NULL, NULL),
(4, 6, 'ExpertWiki', '2b502ca758f765354bb8815fc758906dcb81134f', 'expertwiki@ubuntu-fr.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 'Francais', 'Air', 0, NULL, NULL, NULL, NULL, 1636829264, '172.18.0.3', 1636829265, NULL, NULL, NULL),
(5, 4, 'user', '0f7d0d088b6ea936fb25b477722d734706fe8b40', 'user@ubuntu-fr.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 'Francais', 'Air', 0, NULL, NULL, NULL, NULL, 1636833111, '172.18.0.3', 1636833112, NULL, NULL, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `forum_bans`
--
ALTER TABLE `forum_bans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forum_bans_username_idx` (`username`(25));

--
-- Index pour la table `forum_categories`
--
ALTER TABLE `forum_categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `forum_censoring`
--
ALTER TABLE `forum_censoring`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `forum_config`
--
ALTER TABLE `forum_config`
  ADD PRIMARY KEY (`conf_name`);

--
-- Index pour la table `forum_forums`
--
ALTER TABLE `forum_forums`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `forum_forum_perms`
--
ALTER TABLE `forum_forum_perms`
  ADD PRIMARY KEY (`group_id`,`forum_id`);

--
-- Index pour la table `forum_forum_subscriptions`
--
ALTER TABLE `forum_forum_subscriptions`
  ADD PRIMARY KEY (`user_id`,`forum_id`);

--
-- Index pour la table `forum_groups`
--
ALTER TABLE `forum_groups`
  ADD PRIMARY KEY (`g_id`);

--
-- Index pour la table `forum_online`
--
ALTER TABLE `forum_online`
  ADD UNIQUE KEY `forum_online_user_id_ident_idx` (`user_id`,`ident`(25)),
  ADD KEY `forum_online_ident_idx` (`ident`(25)),
  ADD KEY `forum_online_logged_idx` (`logged`);

--
-- Index pour la table `forum_posts`
--
ALTER TABLE `forum_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forum_posts_topic_id_idx` (`topic_id`),
  ADD KEY `forum_posts_multi_idx` (`poster_id`,`topic_id`);

--
-- Index pour la table `forum_reports`
--
ALTER TABLE `forum_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forum_reports_zapped_idx` (`zapped`);

--
-- Index pour la table `forum_search_cache`
--
ALTER TABLE `forum_search_cache`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forum_search_cache_ident_idx` (`ident`(8));

--
-- Index pour la table `forum_search_matches`
--
ALTER TABLE `forum_search_matches`
  ADD KEY `forum_search_matches_word_id_idx` (`word_id`),
  ADD KEY `forum_search_matches_post_id_idx` (`post_id`);

--
-- Index pour la table `forum_search_words`
--
ALTER TABLE `forum_search_words`
  ADD PRIMARY KEY (`word`),
  ADD KEY `forum_search_words_id_idx` (`id`);

--
-- Index pour la table `forum_topics`
--
ALTER TABLE `forum_topics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forum_topics_forum_id_idx` (`forum_id`),
  ADD KEY `forum_topics_moved_to_idx` (`moved_to`),
  ADD KEY `forum_topics_last_post_idx` (`last_post`),
  ADD KEY `forum_topics_first_post_id_idx` (`first_post_id`);

--
-- Index pour la table `forum_topic_subscriptions`
--
ALTER TABLE `forum_topic_subscriptions`
  ADD PRIMARY KEY (`user_id`,`topic_id`);

--
-- Index pour la table `forum_users`
--
ALTER TABLE `forum_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `forum_users_username_idx` (`username`(25)),
  ADD KEY `forum_users_registered_idx` (`registered`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `forum_bans`
--
ALTER TABLE `forum_bans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `forum_categories`
--
ALTER TABLE `forum_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `forum_censoring`
--
ALTER TABLE `forum_censoring`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `forum_forums`
--
ALTER TABLE `forum_forums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `forum_groups`
--
ALTER TABLE `forum_groups`
  MODIFY `g_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `forum_posts`
--
ALTER TABLE `forum_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `forum_reports`
--
ALTER TABLE `forum_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `forum_search_words`
--
ALTER TABLE `forum_search_words`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `forum_topics`
--
ALTER TABLE `forum_topics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `forum_users`
--
ALTER TABLE `forum_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
