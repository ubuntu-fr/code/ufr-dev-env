include .env
color_method = \033[93m
#containers = ufr_phpmyadmin ufr_db ufr_hugo ufr_dokuwiki ufr_flarum
containers = ufr_phpmyadmin ufr_db ufr_hugo ufr_dokuwiki
app_name = ufr-dev-env

help: ## Affiche ce message d'aide
	@printf "\n$(color_method)help\033[0m\n\n"
	@awk -F ':|##' '/^[^\t].+?:.*?##/ {\
		printf "\033[36m%-30s\033[0m %s\n", $$1, $$NF \
	}' $(MAKEFILE_LIST) | sort
	@printf "\n"

install: ## Installe l'application
	git clone git@gitlab.com:ubuntu-fr/code/ufr-cms.git --recurse-submodules
	git clone git@gitlab.com:ubuntu-fr/code/ufr-doc.git --recurse-submodules

test-data: create-network data db ## Installe les données de test

create-network:
	@if [ -z "$(shell docker network ls -q -f name=web)" ]; then \
		echo "web network does not exist, creating..."; \
		docker network create web; \
	fi;

start: create-network ## Démarre l'application
	@if [ -z "$(shell docker ps -qa -f name=traefik)" ]; then \
		echo "traefik does not exist, creating..."; \
		docker compose up -d traefik; \
	elif [ -z "$(shell docker ps -q -f name=traefik)" ]; then \
		echo "traefik is stopped, starting..."; \
		docker start traefik; \
	else \
		echo "traefik is already running, skipping."; \
	fi;
	@if [ -z "$(shell docker ps -qa -f name=mailhog)" ]; then \
		echo "mailhog does not exist, creating..."; \
		docker compose up -d mailhog; \
	elif 	[ -z "$(shell docker ps -q -f name=mailhog)" ]; then \
		echo "mailhog is stopped, starting..."; \
		docker start mailhog; \
	else \
		echo "mailhog is already running, skipping."; \
	fi;
	@echo "starting ${app_name} containers..."
	@docker compose up -d $(containers)
	@echo "${app_name} started, made available at http://ufr-cms.localhost/"

stop: ## Arrête l'application
	docker compose stop

db: ## Importe la base de données
	docker compose pull ufr_db
	docker compose up -d ufr_db
	sleep 15
	docker exec -i ufr_db mysql -uroot -p${MYSQL_ROOT_PASSWORD} fluxbb < mariadb/fluxbb.sql
	docker compose stop ufr_db

data: ## Importe les données du wiki
	rm -rf ufr-doc/data
	git clone git@gitlab.com:ubuntu-fr/code/ufr-doc-fixtures.git ufr-doc/data

logs: ## Affiche le journal en temps réel
	docker compose logs -f

update: ## Met à jour les containers locaux
	docker compose build --no-cache --pull
	docker compose pull

pull: ## Récupère les modifications poussées en production
	git pull
	git submodule update --init --recursive

clean: stop ## Supprime les logs et les containers de l'application
	sh -c "truncate -s 0 /var/lib/docker/containers/**/*-json.log"
	docker container rm -f $(containers)

reset: stop ## Réinitialise et supprime les modifications
	rm -rf ufr-doc ufr-cms mariadb/data
