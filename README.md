# Environnement de développement pour l'intégralité du site [www.ubuntu-fr.org](https://www.ubuntu-fr.org/)

> **_NOTE :_**  Pas de forum sur la branche _main_, voir la branche [_flarum_](https://gitlab.com/ubuntu-fr/code/ufr-dev-env/-/tree/flarum) pour tester le nouveau forum.

## Déploiement local

### Installer Docker avec Docker Compose

- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/linux/)

(sur Ubuntu il suffit d'installer les paquets [docker.io](apt://docker.io) et [docker-compose-plugin](apt://docker-compose-plugin))

### Cloner le dépôt de sources

```sh
git clone https://gitlab.com/ubuntu-fr/code/ufr-dev-env.git
```

### Installer et démarrer l'application

```sh
cd ufr-dev-env
make install
```
importer les données de test :
```sh
make test-data
```
lancer l'application :
```sh
make start
```

### C'est prêt

- http://ufr-cms.localhost/
- compte administrateur : _admin_ / _admini_
- compte administrateur wiki : _AdminWiki_ / _AdminWiki_
- compte expert wiki : _ExpertWiki_ / _ExpertWiki_
- compte utilisateur : _user_ / _simple_
- liste des commandes disponibles : `make help`
